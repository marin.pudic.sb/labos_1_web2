const express = require('express');
const path = require('path');
const app = express();

// Dobijanje trenutnog direktorijuma
const currentDirectory = process.cwd();

// Postavljanje relativne putanje do 'dist' direktorijuma
const distDirectory = 'dist/competition-app';

// Serve the static files from the 'dist' directory
app.use(express.static(path.join(currentDirectory, distDirectory)));

// Route all other requests to the Angular app's 'index.html'
app.get('*', (req, res) => {
  res.sendFile(path.join(currentDirectory, distDirectory, 'index.html'));
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
