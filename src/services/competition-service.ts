import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CompetitionService {
  
  private competitionsStorageKey = 'competitions';
  private resultsStorageKey = 'results';
  private results: any[] = [];
  private competitions: any[] = [];

  constructor() {  const storedResults = localStorage.getItem(this.resultsStorageKey);
    const storedCompetitions = localStorage.getItem(this.competitionsStorageKey);

    if (storedResults) {
      this.results = JSON.parse(storedResults);
    } else {
      this.results = [
        {
          competitionId: 1,
          results: [
            { participant: 'Team A', score: 2 },
            { participant: 'Team B', score: 1 },
            { participant: 'Team C', score: 0 },
            { participant: 'Team D', score: 3 },
          ],
        },
        {
          competitionId: 2,
          results: [
            { participant: 'Player 1', score: 1.5 },
            { participant: 'Player 2', score: 1.5 },
            { participant: 'Player 3', score: 0.5 },
            { participant: 'Player 4', score: 1 },
          ],
        }
      ];
    }

    if (storedCompetitions) {
      this.competitions = JSON.parse(storedCompetitions);
    } else {
      this.competitions = [
        {
          id: 1,
          name: 'Soccer Tournament',
          participants: 'Team A, Team B, Team C, Team D',
          scoringSystem: '3/1/0',
        },
        {
          id: 2,
          name: 'Chess Championship',
          participants: 'Player 1, Player 2, Player 3, Player 4',
          scoringSystem: '1/0.5/0',
        },
      ];
    }

    this.saveResultsToLocalStorage(this.results);
    this.saveCompetitionsToLocalStorage(this.competitions);
  }

  createCompetition(competition: any) {
    const competitions = this.getCompetitionsFromLocalStorage();
    competitions.push(competition);
    this.saveCompetitionsToLocalStorage(competitions);
  }

  submitResult(result: any) {
    const results = this.getResultsFromLocalStorage();
    results.push(result);
    this.saveResultsToLocalStorage(results);
  }

  getCompetitions() {
    return this.getCompetitionsFromLocalStorage();
  }

  getResults() {
    return this.getResultsFromLocalStorage();
  }

  public getCompetitionsFromLocalStorage(): any[] {
    const storedCompetitions = localStorage.getItem(this.competitionsStorageKey);
    return storedCompetitions ? JSON.parse(storedCompetitions) : [];
  }

  private saveCompetitionsToLocalStorage(competitions: any[]) {
    localStorage.setItem(this.competitionsStorageKey, JSON.stringify(competitions));
  }

  private getResultsFromLocalStorage(): any[] {
    const storedResults = localStorage.getItem(this.resultsStorageKey);
    return storedResults ? JSON.parse(storedResults) : [];
  }

  private saveResultsToLocalStorage(results: any[]) {
    localStorage.setItem(this.resultsStorageKey, JSON.stringify(results));
  }

  getCompetitionById(competitionId: number): any {7
    var competitions = this.getCompetitionsFromLocalStorage();
    return competitions.find(competition => competition.id == competitionId);
  }
}
