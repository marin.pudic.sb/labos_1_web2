import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CompetitionService } from 'src/services/competition-service';

@Component({
  selector: 'app-competition-create',
  templateUrl: './competition-create.component.html',
  styleUrls: ['./competition-create.component.css']
})
export class CompetitionCreateComponent {
  competitionForm: FormGroup;
  schedule: string[] | undefined;
  hometeams: string | undefined;
  localStorage: Storage;
  initialScores: { competitionId: number; results: []} = {
    competitionId: 0,
    results: []
  };
  scoreMap = new Array<any>();
  constructor(private fb: FormBuilder, private router: Router, private competitionService: CompetitionService) {
    this.competitionForm = this.fb.group({
      id: [this.generateUniqueID()],
      name: ['', Validators.required],
      participants: ['',[Validators.required, Validators.pattern(/^([a-zA-Z\s]+, ){2,6}[a-zA-Z\s]+$/)
    ]],
      scoringSystem: ['', [Validators.pattern(/^\d+\/\d+\/\d+$/)]],
    });
    this.localStorage = window.localStorage;
   
  }

  onSubmit() {
    if (this.competitionForm.valid) {
      this.competitionService.createCompetition(this.competitionForm.value)
      const participants = this.competitionForm.value.participants.split(',').map((p: string) => p.trim());
      const initialResults = participants.map((participant: string) => ({ participant, score: 0 }));
      this.initialScores.competitionId = this.competitionForm.get('id')?.value
      this.initialScores.results = initialResults;
      const storedData = this.localStorage.getItem('results')

      if(storedData){      
        this.scoreMap = JSON.parse(storedData);
        this.scoreMap.push(this.initialScores)
      }
      
      this.localStorage.setItem('results', JSON.stringify(this.scoreMap));
      this.generateSchedule();
      this.showSuccessMessage()
    }else {
      alert('Insert correct data.');
    }
  }

  showSuccessMessage() {
    alert('Competition successfully created!');
  }

  generateUniqueID(): number {
    let uniqueID: number;
      uniqueID = Math.floor(Math.random() * 10000);   return uniqueID;
  }

  goBack() {
    this.router.navigate(['/navigation']); 
  }

  generateSchedule() {
    const participants = this.competitionForm.get('participants')?.value.split(',').map((p: string) => p.trim());
    this.schedule = this.createRoundRobin(participants);
  }

   createRoundRobin(participants: string[]): string[] {
    if (participants.length < 2 || participants.length % 2 !== 0) {
      return ['At least 2 participants are needed or the number of participants has to be even.'];
    }
  
    const n = participants.length;
    const schedule: string[] = [];
    const rounds = n - 1;
  
    for (let round = 1; round <= rounds; round++) {
      schedule.push(`Round ${round}:`);
      for (let i = 0; i < n / 2; i++) {
        const team1 = participants[i];
        const team2 = participants[n - 1 - i];
        schedule.push(`Match ${i + 1}: ${team1} - ${team2}`);
      }
  
      const firstTeam = participants[0];
      participants.shift(); 
      participants.push(firstTeam); 
    }
  
    return schedule;
  }
  
  
  
  
  
  
  

  }

