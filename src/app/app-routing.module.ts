import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompetitionCreateComponent } from './competition-create/competition-create.component';
import { ResultEntryComponent } from './result-entry/result-entry.component';
import { RankingsComponent } from './rankings/rankings.component';
import { LoginComponent } from './login/login.component';
import { CallbackComponent } from './callback/callback.component';
import { NavigationComponent } from './navigation/navigation.component';
import { CompetitionDetailComponent } from './competition-detail/competition-detail.component';

const routes: Routes = [
  { path: 'create-competition', component: CompetitionCreateComponent },
  { path: 'enter-results', component: ResultEntryComponent },
  { path: 'view-rankings', component: RankingsComponent },
  { path :'login', component:LoginComponent},
  {path:'callback', component:CallbackComponent},
  {path:'navigation', component:NavigationComponent},
  { path: 'competition/:id', component: CompetitionDetailComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' }, // Default route
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
