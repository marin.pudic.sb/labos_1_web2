import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AuthModule } from '@auth0/auth0-angular';
import { AppComponent } from './app.component';
import { CompetitionCreateComponent } from './competition-create/competition-create.component';
import { ResultEntryComponent } from './result-entry/result-entry.component';
import { RankingsComponent } from './rankings/rankings.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms'; // for template-driven forms
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { CallbackComponent } from './callback/callback.component';
import { NavigationComponent } from './navigation/navigation.component';
import { MatIconModule, MatListModule } from '@angular/material';
import { CompetitionDetailComponent } from './competition-detail/competition-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    CompetitionCreateComponent,
    ResultEntryComponent,
    RankingsComponent,
    LoginComponent,
    CallbackComponent,
    NavigationComponent,
    CompetitionDetailComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    FormsModule,
    MatListModule,
    RouterModule,
    MatIconModule,
    HttpClientModule ,
    AuthModule.forRoot({
      domain: 'dev-b7636g3yepfufanh.eu.auth0.com',  
      clientId: 'fOxVdeeO65WAQk9H96ISO78BQgiCZQ7z',  
      authorizationParams: {
        redirect_uri: window.location.origin+'/callback'
      }
    }),
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
