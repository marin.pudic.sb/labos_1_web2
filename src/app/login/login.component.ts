import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  ngOnInit(): void {
  }

  constructor(public auth: AuthService, private router: Router) { }

  login() {
    this.auth.loginWithRedirect();

  }

  logout() {
    this.auth.logout();
  }
}
