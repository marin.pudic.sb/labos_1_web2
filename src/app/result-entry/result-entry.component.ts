import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CompetitionService } from 'src/services/competition-service';

@Component({
  selector: 'app-result-entry',
  templateUrl: './result-entry.component.html',
  styleUrls: ['./result-entry.component.css']
})
export class ResultEntryComponent implements OnInit {
  resultForm: FormGroup;
  availableCompetitions: any;
  scoreMap = new Array<any>();
  localStorage: Storage;

  constructor(private fb: FormBuilder, private router: Router, private competitionService: CompetitionService) {
    this.resultForm = this.fb.group({
      competitionId: [, Validators.required],
      results: this.fb.array([]),
    });

    this.localStorage = window.localStorage;
  }

  ngOnInit(): void {
    this.availableCompetitions = this.competitionService.getCompetitions();
  }

  addResult() {
    const resultGroup = this.fb.group({
      participant: [''],
      score: [''],
    });
    this.resultArray.push(resultGroup);
  }

  get resultArray() {
    return this.resultForm.get('results') as FormArray;
  }

  removeResult(index: number) {
    this.resultArray.removeAt(index);
  }

  onSubmit() {
    const resultsArray = this.resultForm.get('results') as FormArray;

    if (resultsArray && resultsArray.length > 0) {
      const competitionId = parseInt(this.resultForm.get('competitionId')?.value, 10); 
      const storedData = this.localStorage.getItem('results');

      if (storedData) {
        this.scoreMap = JSON.parse(storedData);
        for(let res of this.scoreMap){
          if(res.competitionId == competitionId ){
            resultsArray.controls.forEach((resultControl) => {
              const participantControl = resultControl.get('participant')?.value;
              const scoreControl = resultControl.get('score')?.value;
      
              if (participantControl && scoreControl) {
                const participantName = participantControl;
                const score = parseFloat(scoreControl);
                for(let res2 of res.results){
                  if(res2.participant == participantName){
                    res2.score = res2.score + score
                  }
                }
                }
              }
            );
          }
        }
      }

      this.localStorage.setItem('results', JSON.stringify(this.scoreMap));
      this.resultForm.reset();
    }
  }

  goBack() {
    this.router.navigate(['/navigation']);
  }
}
