import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-callback',
  template: `
    <div>
      <h3>Loading...</h3>
    </div>
  `,
})

export class CallbackComponent implements OnInit {
  constructor(public auth: AuthService, private router: Router) {}

  ngOnInit() {
    this.auth.isAuthenticated$.subscribe(() => {
      this.router.navigate(['/navigation']); 
    });
  }
}
