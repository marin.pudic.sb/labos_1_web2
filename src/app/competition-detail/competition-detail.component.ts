import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompetitionService } from 'src/services/competition-service';

@Component({
  selector: 'app-competition-detail',
  templateUrl: './competition-detail.component.html',
  styleUrls: ['./competition-detail.component.css']
})
export class CompetitionDetailComponent implements OnInit {
  competition: any;
  competitions: any[] = [];
  results: any[]=[];
  result_help: any[]=[];
  selectedCompetition: any;
  filteredResults: any[] = [];
  schedule: string[] = [];
  constructor(
    private route: ActivatedRoute,
    private competitionService: CompetitionService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const competitionId = params['id'];
      this.competition = this.competitionService.getCompetitionById(competitionId);

      this.results = this.competitionService.getResults();
      this.filteredResults = this.results.filter(result => result.competitionId === this.competition.id);
      this.generateSchedule(this.competition);

    });
  }

 generateSchedule(competition: any) {
    const participants = competition.participants.split(',').map((p: string) => p.trim());
    this.schedule = this.createRoundRobin(participants);
  }

  createRoundRobin(participants: string[]): string[] {
    if (participants.length < 2 || participants.length % 2 !== 0) {
      return ['At least 2 participants are needed or the number of participants has to be even.'];
    }
  
    const n = participants.length;
    const schedule: string[] = [];
    const rounds = n - 1;
  
    for (let round = 1; round <= rounds; round++) {
      schedule.push(`Round ${round}:`);
      for (let i = 0; i < n / 2; i++) {
        const team1 = participants[i];
        const team2 = participants[n - 1 - i];
        schedule.push(`Match ${i + 1}: ${team1} - ${team2}`);
      }
  
      const firstTeam = participants[0];
      participants.shift(); 
      participants.push(firstTeam);
    }
  
    return schedule;
  }

}
