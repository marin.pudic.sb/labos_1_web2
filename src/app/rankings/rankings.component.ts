import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CompetitionService } from 'src/services/competition-service';

@Component({
  selector: 'app-rankings',
  templateUrl: './rankings.component.html',
  styleUrls: ['./rankings.component.css']
})
export class RankingsComponent implements OnInit {
  competitions: any[] = [];
  results: any[]=[];
  result_help: any[]=[];
  selectedCompetition: any;
  filteredResults: any[] = [];
  schedule: string[] = [];
  constructor(private competitionService: CompetitionService, private router: Router) {}

  ngOnInit(): void {
    this.competitions = this.competitionService.getCompetitions();
    this.results = this.competitionService.getResults();
  }

  showCompetitionRankings(competition: any) {
    this.selectedCompetition = competition;
  }

  getCompetitions() {
    return this.competitionService.getCompetitions();
  }

  getResults() {
    return this.competitionService.getResults();
  }

  goBack() {
    this.router.navigate(['/navigation']); 
  }
}
